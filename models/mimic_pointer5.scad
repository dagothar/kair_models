difference() {
    union() {
        cylinder(d=25, h=10, $fn=100);
        
        translate([0, 0, 10])
            rotate([90, 0, 0]) translate([0, 0, -10]) linear_extrude(height=20) {
                polygon(points=[
                    [-6, 0],
                    [-2, 4],
                    [-1, 37],
                    [1, 37],
                    [2, 4],
                    [6, 0]
                ]);
            }
    }
    
    for (a=[0:120:2*360]) {
        translate([8*cos(a), 8*sin(a), 0])
            cylinder(d=7.3, h=3, $fn=100);
        
        translate([8*cos(a+60), 8*sin(a+60), 0])
            cylinder(d=5.15, h=5, $fn=100);
    }
    
    #translate([0, 0, 47]) rotate([90, 0, 90]) translate([0, 0, -5]) linear_extrude(height=10) {
        polygon(points=[
            [-10, 0], [0, -2], [10, 0]
        ]);
    }
}