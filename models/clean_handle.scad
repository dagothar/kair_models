union() {
    
    translate([0, 0, 0])
        cylinder(d=25, h=110, $fn=100);
    
    translate([-25/2+1, -25/2+1, 100+1])
        minkowski() {
            cube([48, 23, 23], center=false);
            sphere(1, $fn=20);
        }
    
    
}