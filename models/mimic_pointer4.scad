difference() {
    union() {
        cylinder(d=25, h=10, $fn=100);
        
        //translate([0, 0, 10])
        //    cube([9.8, 9.8, 10], center=true);
        for (a=[0:120:2*360]) {
            translate([8*cos(a+60), 8*sin(a+60), 5])
                cylinder(d=4.9, h=5, $fn=100);
        }
        
        translate([0, 0, 10])
            rotate([90, 0, 0]) translate([0, 0, -10]) linear_extrude(height=20) {
                polygon(points=[
                    [-6, 0],
                    [-2, 4],
                    [-2, 33],
                    [0, 35],
                    [2, 33],
                    [2, 4],
                    [6, 0]
                ]);
            }
    }
    
    for (a=[0:120:2*360]) {
        translate([8*cos(a), 8*sin(a), 0])
            cylinder(d=7.3, h=3, $fn=100);
        
        translate([8*cos(a+60), 8*sin(a+60), 0])
            cylinder(d=5.1, h=5, $fn=100);
    }
}