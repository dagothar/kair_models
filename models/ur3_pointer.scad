module mount_ur3() {
    sx = 25 * cos(45);
    difference() {
        union() {
            translate([0, 0, 0])
                cylinder(d=63, h=5, $fn=100);
            translate([0, 0, 5])
                cylinder(d=31.5, h=5, $fn=100);
            translate([25, 0, 5])
                cylinder(d=6, h=5, $fn=100);
        }
        union() {
            translate([sx, sx, 0])
                cylinder(d=6, h=5, $fn=100);
            translate([-sx, sx, 0])
                cylinder(d=6, h=5, $fn=100);
            translate([sx, -sx, 0])
                cylinder(d=6, h=5, $fn=100);
            translate([-sx, -sx, 0])
                cylinder(d=6, h=5, $fn=100);
        }
    }
}

union() {
    translate([0, 0, 10]) rotate([180, 0, 0])
        mount_ur3();
    translate([0, 0, 10])
        cylinder(d1=25, d2=15, h=10, $fn=100);
    translate([0, 0, 10])
        cylinder(d=15, h=85, $fn=100);
    translate([0, 0, 95])
        cylinder(d1=15, d2=1, h=10, $fn=100);
}
