
difference() {
union() {
difference() {
    translate([5, 0, 7.5]) rotate([0, 0, 0])
        cube([125, 106, 15], center=true);
    union() {
        translate([0, 0, 12.5]) rotate([0, 0, 0])
            cube([105, 96, 5], center=true);
        translate([115/2, 0, 15]) rotate([0, 0, 0])
            cube([50, 16, 10], center=true);
        translate([105/2, 25, 15]) rotate([0, 0, 0])
            cylinder(d=8, h=100, center=true, $fn=100);
        translate([105/2, -25, 15]) rotate([0, 0, 0])
            cylinder(d=8, h=100, center=true, $fn=100);
        translate([105/2, 25, 15]) rotate([0, 0, 0])
            cylinder(d=20, h=10, center=true, $fn=100);
        translate([105/2, -25, 15]) rotate([0, 0, 0])
            cylinder(d=20, h=10, center=true, $fn=100);
    }
}

    translate([145/2, 0, 5]) cube([10, 20, 10], center=true);
    translate([155/2, 0, 0]) cylinder(d=20, h=10, $fn=100);

    translate([-125/2, 0, 5]) cube([10, 20, 10], center=true);
    translate([-135/2, 0, 0]) cylinder(d=20, h=10, $fn=100);
}

    union() {
        translate([155/2, 0, 0]) cylinder(d=8, h=10, $fn=100);
        translate([155/2, 0, 2]) cylinder(d=13, h=8, $fn=100);
        translate([-135/2, 0, 0]) cylinder(d=8, h=10, $fn=100);
        translate([-135/2, 0, 2]) cylinder(d=13, h=8, $fn=100);
    }
}