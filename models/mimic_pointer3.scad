difference() {
    union() {
        cylinder(d=25, h=10, $fn=100);
        
        translate([0, 0, 10]) rotate_extrude($fn=100) {
            polygon(points=[[0, 0], [0, 35], [1, 34], [5, 5], [10, 0]]);
        }
    }
    
    for (a=[0:120:2*360]) {
        translate([8*cos(a), 8*sin(a), 0])
            cylinder(d=7.3, h=3, $fn=100);
        
        translate([8*cos(a+60), 8*sin(a+60), 0])
            cylinder(d=5.15, h=5, $fn=100);
    }
}