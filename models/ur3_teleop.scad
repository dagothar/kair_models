module mount_ur3() {
    sx = 25 * cos(45);
    difference() {
        union() {
            translate([0, 0, 0])
                cylinder(d=63, h=5, $fn=100);
            translate([0, 0, 5])
                cylinder(d=31.5, h=5, $fn=100);
            translate([25, 0, 5])
                cylinder(d=6, h=5, $fn=100);
        }
        union() {
            translate([sx, sx, 0])
                cylinder(d=6, h=5, $fn=100);
            translate([-sx, sx, 0])
                cylinder(d=6, h=5, $fn=100);
            translate([sx, -sx, 0])
                cylinder(d=6, h=5, $fn=100);
            translate([-sx, -sx, 0])
                cylinder(d=6, h=5, $fn=100);
        }
    }
}

difference() {
    union() {
        translate([0, 0, 10]) rotate([180, 0, 0])
            mount_ur3();
        translate([0, 0, 10])
            cylinder(d=25, h=10, $fn=100);
        translate([0, 0, 10])
            cylinder(d=15, h=50, $fn=100);
    }
    union() {
        translate([0, 0, 10])
            cylinder(d=7, h=50, $fn=100);
        translate([0, 0, 45])
            cube([30, 2, 50], center=true);
    }
}