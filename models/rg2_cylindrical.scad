module base() {
    union() {
            translate([0, -0.02/2, 0])
                cube([0.03-0.01, 0.02, 0.0115], center=false);
            translate([0.03-0.01, 0, 0])
                cylinder(h=0.0115, r=0.01, center=false, $fn=100);
    }
}

module finger() {
    difference() {
        union() {
            translate([0, -0.0116/2, 0])
                cube([0.025-0.0058, 0.0116, 0.007], center=false);
            translate([0.025-0.0058, 0, 0])
                cylinder(h=0.007, r=0.0058, center=false, $fn=100);
        }
        union() {
            translate([0, 0.0038, 0.002])
                cube([0.025, 0.004, 0.003], center=false);
            translate([0, -0.0078, 0.002])
                cube([0.025, 0.004, 0.003], center=false);
        }
    }
}

module fingertip() {
    difference() {
        translate([0, 0, 0])
            base();
        union() {
            translate([0, 0, 0.0045])
                finger();
            translate([0.005, 0, 0]) rotate([0, 90, 0])
                cylinder(h=0.05, r=0.0035, $fn=100);
        }
    }
    
}

scale(1000)
fingertip();