difference() {

    union() {
        minkowski() {
            translate([0, 0, 0])
                cube([48, 48, 8], center=true);
            cylinder(r=1, z=2, $fn=10, center=true);
        }
        
        minkowski() {
            translate([40.5, 0, 0])
                cube([39, 39.5, 8], center=true);
            cylinder(r=1, z=2, $fn=10, center=true);
        }
    }
    
    union() {
        minkowski() {
            translate([0, 0, 0])
                cube([38, 38, 8], center=true);
            cylinder(r=1, z=2, $fn=10, center=true);
        }
    }

}