union() {


difference() {
    union() {
        translate([0, 0, 0])
            cylinder(d1=15, d2=15, h=10, $fn=100);
        translate([0, 0, 0])
            cylinder(d=15, h=50, $fn=100);
    }
    union() {
        translate([0, 0, 10])
            cylinder(d=8, h=50, $fn=100);
        translate([0, 0, 40])
            cube([30, 2, 50], center=true);
    }
}

    difference() {
        translate([0, 0, -20])
            cylinder(d1=15, d2=15, h=20, $fn=100);
        
        union() {
            translate([0, 0, -20])
                cube([30, 2, 30], center=true);
            translate([-4, -4, -20])
                cube([8, 8, 20], center=false);
        }
    }



}