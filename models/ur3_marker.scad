
    sx = 25 * cos(45);
    translate([0, 0, 10]) rotate([180, 0, 0])
    difference() {
        union() {
            translate([0, 0, 0])
                cylinder(d=63, h=5, $fn=100);
            translate([0, 0, 5])
                cylinder(d=31.5, h=5, $fn=100);
            translate([25, 0, 5])
                cylinder(d=6, h=5, $fn=100);
            translate([0, 0, 0])
                cube([90, 90, 5], center=true);
        }
        union() {
            translate([sx, sx, -50])
                cylinder(d=6, h=100, $fn=100);
            translate([-sx, sx, -50])
                cylinder(d=6, h=100, $fn=100);
            translate([sx, -sx, -50])
                cylinder(d=6, h=100, $fn=100);
            translate([-sx, -sx, -50])
                cylinder(d=6, h=100, $fn=100);
            
            translate([sx, sx, -2.5])
                cylinder(d=10, h=2, $fn=100);
            translate([sx, -sx, -2.5])
                cylinder(d=10, h=2, $fn=100);
            translate([-sx, sx, -2.5])
                cylinder(d=10, h=2, $fn=100);
            translate([-sx, -sx, -2.5])
                cylinder(d=10, h=2, $fn=100);
        }
    }


