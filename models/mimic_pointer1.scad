difference() {
    union() {
        cylinder(d=28, h=5, $fn=100);
        
        //translate([0, 0, 10])
        //    cube([9.8, 9.8, 10], center=true);
        for (a=[0:120:2*360]) {
            translate([8*cos(a+60), 8*sin(a+60), 5])
                cylinder(d=4.85, h=5, $fn=100);
        }
    }
    
    for (a=[0:120:2*360]) {
        translate([8*cos(a), 8*sin(a), 0])
            cylinder(d=5.6, h=100, $fn=100);
        
        translate([8*cos(a), 8*sin(a), 3])
            cylinder(d=7.3, h=100, $fn=100);
    }
}