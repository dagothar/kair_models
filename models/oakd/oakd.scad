union() {
    // plytka
    translate([0, 0, -0.00163/2])
    cube([0.110, 0.06, 0.00163], center=true);
    
    // radiator
    translate([-0.014, 0.006, 0.01])
        cube([0.04, 0.03, 0.02], center=true);
}