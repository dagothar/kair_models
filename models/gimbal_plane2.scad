ring_outer_diameter = 50;
ring_inner_diameter = 40;
ring_h = 5;
spike_length = 7;
pit_depth = 2;
spike_diameter = 0.75;

dd = ring_outer_diameter - ring_inner_diameter;

difference () {
    translate([20.5, 0, 0])
        rotate([-90, 0, 0])
        rotate([0, 0, 90])
        rotate([-90, 0, 0])
        scale(0.55)
            import("plane1.stl", convexity=3);
    
union() {
    
    
    difference() {
        translate([0, 0, -ring_h/2])
            cylinder(d=ring_outer_diameter, h=ring_h, $fn=100);
        
        translate([0, 0, -ring_h/2-0.5])
            cylinder(d=ring_inner_diameter, h=ring_h+1, $fn=100);
        
        translate([0, -ring_inner_diameter/2, 0]) rotate([90, 0, 0])
            cylinder(r1 = ring_h/2, r2=spike_diameter, h=pit_depth, $fn=100);
        translate([0, 0, 0]) rotate([90, 0, 0])
            cylinder(r = ring_h/2, h=ring_inner_diameter/2, $fn=100);
        
        #translate([0, ring_inner_diameter/2, 0]) rotate([-90, 0, 0])
            cylinder(r1 = ring_h/2, r2=spike_diameter, h=pit_depth, $fn=100);
        translate([0, 0, 0]) rotate([-90, 0, 0])
            cylinder(r = ring_h/2, h=ring_inner_diameter/2, $fn=100);
    }
    
    translate([ring_outer_diameter/2, 0, 0]) rotate([0, 90, 0])
        cylinder(r1 = ring_h/2, r2=spike_diameter, h=spike_length, $fn=100);
    
    translate([ring_inner_diameter/2, 0, 0]) rotate([0, 90, 0])
        cylinder(r = ring_h/2, h=dd/2, $fn=100);
    
    translate([-ring_outer_diameter/2, 0, 0]) rotate([0, -90, 0])
        cylinder(r1 = ring_h/2, r2=spike_diameter, h=spike_length, $fn=100);
    
    translate([-ring_outer_diameter/2, 0, 0]) rotate([0, 90, 0])
        cylinder(r = ring_h/2, h=dd/2, $fn=100);
}
}

