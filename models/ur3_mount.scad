module mount_ur3() {
    sx = 25 * cos(45);
    difference() {
        union() {
            translate([0, 0, 0])
                cylinder(d=63, h=5, $fn=100);
            translate([0, 0, 5])
                cylinder(d=31.5, h=5, $fn=100);
            translate([25, 0, 5])
                cylinder(d=6, h=5, $fn=100);
        }
        union() {
            translate([sx, sx, 0])
                cylinder(d=6, h=5, $fn=100);
            translate([-sx, sx, 0])
                cylinder(d=6, h=5, $fn=100);
            translate([sx, -sx, 0])
                cylinder(d=6, h=5, $fn=100);
            translate([-sx, -sx, 0])
                cylinder(d=6, h=5, $fn=100);
        }
    }
}

mount_ur3();